import Domain.User;
import Service.UserRegistrationService;
import Service.UserRegistrationServiceImpl;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserResourceTest {

    @Test
    public void testRegisterUserSuccess(){

        UserRegistrationService userRegResource = new UserRegistrationServiceImpl();

        User user = new User();
        user.setUsername("12345");
        user.setFirstName("Daniel");
        user.setLastName("Edmonds");

        String userResponse = userRegResource.validateUser(user);

        assertEquals("User successfully registered", userResponse);

    }
}
