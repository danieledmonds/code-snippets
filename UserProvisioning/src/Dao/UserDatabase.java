package Dao;

import Domain.User;

import java.util.HashMap;

public class UserDatabase {

    private HashMap<String, User> store = new HashMap<>();

    public void putUserStore(User user){

        store.put(user.getUsername(), user);

    }

    public User getUserStore(String userName){
        return store.get(userName);
    }
}
