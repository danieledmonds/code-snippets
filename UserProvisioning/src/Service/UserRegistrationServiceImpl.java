package Service;

import Dao.UserDatabase;
import Domain.User;
import Service.UserRegistrationService;

public class UserRegistrationServiceImpl implements UserRegistrationService {

    UserDatabase userDatabase = new UserDatabase();

    @Override
    public String validateUser(User user) {

       User userFromDb = userDatabase.getUserStore(user.getUsername());

       if(userFromDb == null){
            userDatabase.putUserStore(user);
       } else if (userFromDb.getUsername().equals(user.getUsername())){
           return "User currently exists in database";
       }


       return "User successfully registered";
    }
}
