package API;

import Domain.User;

public interface UserResourceEndpoint {

    public String registerUser(User user);
}