package API;

import Domain.User;
import Service.UserRegistrationService;
import Service.UserRegistrationServiceImpl;

public class UserResourceEndpointImpl implements UserResourceEndpoint {

    UserRegistrationService userReg = new UserRegistrationServiceImpl();

    @Override
    public String registerUser(User user) {

        return  userReg.validateUser(user);
    }
}
